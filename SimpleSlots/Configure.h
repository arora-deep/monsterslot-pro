//
//  Configure.h
//  PartySlots
//
//  Created by Alex on 7/22/13.
//
//



// -----------------------------------
// GAME SETTINGS
// -----------------------------------
#define INITIAL_STARTUP_COINS @"3000"
#define INITIAL_STARTUP_BET @"200"
#define INITIAL_LINES_COUNT @"20"
#define LABEL_WELCOME @"Welcome to Monster Slots Casino!"
#define LABEL_TAPPLAY @"Tap Spin to Play"

#define DELAY_FOR_AUTO_SPIN 7

#define DONT_SHOW_APPLOVIN_ON_EACH_SCREEN

#define kGameCenterLeaderboardID @"com.aaron.monsterSlotPRO.Leaderboard"

// -----------------------------------
// CUSTOM BUILT-IN DOWNLOAD POPUP
//
// if you want to have a custom popup asking for download of some other game,
// in addition to setting these up you'll also need to change the graphic of the download splash:
// replace images downloadview_small.png and downloadview_small@2x.png with your own
// -----------------------------------
#define SHOW_DOWNLOAD_POPUP NO
#define DOWNLOAD_NOW_URL @"https://itunes.apple.com/us/app/monster-slot-casino-blast/id907323715?ls=1&mt=8&at=10lIwv"



// -----------------------------------
// IN APP PURCHASES
// these must match the product identifiers that you've set up in iTunesConnect
// -----------------------------------
#define IAP1 @"com.aaron.babyStrollerRunnerPro.somecoins"
#define IAP2 @"com.aaron.babyStrollerRunnerPro.manycoins"
#define IAP3 @"com.aaron.babyStrollerRunnerPro.plentyofcoins"
#define IAP4 @"com.aaron.babyStrollerRunnerPro.verymanycoins"
#define IAP5 @"com.aaron.babyStrollerRunnerPro.hugecoins"
#define IAP6 @"com.aaron.babyStrollerRunnerPro.enormouscoins"
// -----------------------------------
// the amounts of coins you buy in each in app purchase
// -----------------------------------


#define IAP_AMT_1 1000
#define IAP_AMT_2 3200
#define IAP_AMT_3 8000
#define IAP_AMT_4 20000
#define IAP_AMT_5 80000
#define IAP_AMT_6 200000


// -----------------------------------
// ADVERTISING SETTINGS:
// -----------------------------------

// playhaven
#define PLAYHAVEN_TOKEN @"7a8e822487b44feb9a2e9e9c3c841059"
#define PLAYHAVEN_SECRET @"9f2acb4b5ac749b78e47b46bcb670ebe"
#define PLAYHAVEN_MORE_GAMES_STRING @"boys_landscape_placements"
// chartboost
#define CHARTBOOST_APP_KEY  @"53cc0bfc1873da2c64f64e3f"
#define CHARTBOOST_APP_SECRET @"910dc416c500654647d22df37cc665cf6ecdb9b9"
// revmob
#define REVMOB_APP_ID @"53d7f09aaf69ad95475a348d"

// if this one is 1, then we'll show the AppLovin interstitial on the lobby every single time we get to the lobby; otherwise, we show it once every N times
#define ADS_INTERSTITIAL_ON_LOBBY_FREQUENCY 5

// if you comment out this line there will be no showing of ads on app resume
#define ADS_RESUME_FREQUENCY 5
/*
 for revmob interstitials instead of Chartboost, in method - (void)applicationWillEnterForeground:(UIApplication *)application of AppDelegate.m,
 instead of the line that says
 [[Chartboost sharedChartboost] showInterstitial];
 write a line that says
 [[RevMobAds session] showFullscreen];
 */

// if you comment out this line there will be no showing of ads on spin during the game
#define ADS_SPIN_FREQUENCY 7
/*
 for revmob interstitials instead of Chartboost, in method  - (void)spin of GameClassic20.m,
 instead of the line that says
 [[Chartboost sharedChartboost] showInterstitial];
 write a line that says
 [[RevMobAds session] showFullscreen];
 */






//#define kSERVER_SIDE_URL @"http://sync1.okduk.com/sync_vegasslots.php"
//#define kURL_VERIFY_PURCHASE_RECEIPT @"http://sync1.okduk.com/transaction.php"