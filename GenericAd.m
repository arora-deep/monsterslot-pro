//
//  GenericAd.m
//  Alien Tower
//
//  Created by Asad Khan on 05/02/2013.
//
//

#import "GenericAd.h"
#import "Chartboost.h"

#import "SNAdsManager.h"
static int count = 0;
static BOOL isAdVisible;
static BOOL isAppLovinWorking;
@interface GenericAd()


@property (nonatomic, retain) NSTimer *playHavenTimer;
@property (nonatomic, assign)BOOL hasPlayHavenAdLoaded;
@end


@implementation GenericAd

/**
Chartboost AdNetwork calls the didFailToLoadInterstitial: delegate method even when it tries and fails to load the cache which results in 2 callbacks. The Problem with our admanager than is it loads two lower priority ads. To mitigate this we will be using nstimer to distinguish between 2 consecutive calls. If the time difference between the calls is less than a specified limit we will ignore the second call.
 For this we will be declaring a static float variable to store first intial failure time and than we will compare it with every second call we wll recieve.

 Another option is to wait a predefined number of seconds before calling another ad network. This option is NOT currently implemented in this code.
*/

static double firstCallBackTime;
static int callBackCount;

/**
 Sometimes RevMob fails to notify that loading ad has been failed so as a back we're adding fail time for RevMob FullScreen and Banner both
 */


@synthesize adNetworkType = _adNetworkType;
@synthesize adType = _adType;
@synthesize isTestAd = _isTestAd;
@synthesize adPriority = _adPriority;
@synthesize adView;
//@synthesize adStarted;
@synthesize revMobFullScreenAd = _revMobFullScreenAd;
@synthesize delegate = _delegate;

@synthesize revMobFullScreenAdTimer = _revMobFullScreenAdTimer;
@synthesize isRevMobFullScreenAlreadyShown = _isRevMobFullScreenAlreadyShown;
@synthesize playHavenTimer = _playHavenTimer;
//@synthesize isTopBannerAd = _isTopBannerAd;


- (id) initWithAdNetworkType:(NSUInteger)adNetworkType andAdType:(NSUInteger)adType{
	self = [super init];
	if(self !=nil){
        _adType = adType;
        _adNetworkType = adNetworkType;
        switch (adNetworkType) {
            case kAppLovin:
                if (_adType == kBannerAd){
                    _adPriority = kAppLovinBannerAdPriority;
                }
                else if (adType == kFullScreenAd){
                    _adPriority = kAppLovinFullScreenAdPriority;
                }
                break;
            case kChartBoost:
                if (adType == kFullScreenAd){
                    _adPriority = kChartBoostFullScreeAdPriority;
                    self.chartBoost = [SNAdsManager sharedManager].chartBoost;
                    self.chartBoost.delegate = self;
                }
                else if (adType == kMoreAppsAd)
                    _adPriority = kChartBoostMoreAppsAd;
                break;
            case kPlayHaven:
                if (adType == kFullScreenAd) {
                    _adPriority = kPlayHavenFullScreenAdPriority;
//                    [[PHPublisherContentRequest requestForApp:kPlayHavenAppToken secret:kPlayHavenSecret placement:kPlayHavenPlacement delegate:self] preload];
                }
                break;
            case kRevMob:
                if(adType == kBannerAd){
                    _adPriority = kRevMobBannerAdPriority;
                }
                else if (adType == kFullScreenAd){
                    _adPriority = kRevMobFullScreenAdPriority;
                    [_revMobFullScreenAd retain];
                    //_revMobFullScreenAd.delegate = self;
                    [_revMobFullScreenAd loadWithSuccessHandler:nil andLoadFailHandler:^(RevMobFullscreen *fs, NSError *error){
                          //  [self.delegate revMobFullScreenDidFailToLoad:self];
                    }];   
                }
                else if (adType == kButtonAd)
                    _adPriority = kRevMobButtonAdPriority;
                else if (adType == kLinkAd){
                    _adPriority = kRevMobLinkAdPriority;
            
                }
                else if (adType == kPopUpAd)
                    _adPriority = kRevMobPopAdPriority;
                else if (adType == kLocalNotificationAd)
                    _adPriority = kRevMobLocalNotificationAdPriority;
                else
                    [NSException raise:@"Invalid Ad Type" format:@"Ad Type is invalid"];
                break;
            default:
               // NSAssert(!adNetworkType == kUndefined, @"Value for Ad Network cannot be Undefined");
                [NSException raise:@"Undefined Ad Network" format:@"Ad Network is unknown cannot continue"];
                break;
        }
	}
	return self;
}

- (id) initWithAdType:(NSUInteger)adType{
    self = [super init];
	if(self !=nil){
        _adType = adType;   
    }
    return self;
}

-(void)showBannerAdAtTop{
    switch(self.adNetworkType){
        case kRevMob:{
            @try {
                dispatch_async(dispatch_get_main_queue(), ^{
//                    NSUInteger screenWidth = [[UIScreen mainScreen] bounds].size.width;
//                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//                        self.revMobBannerAdView.frame = CGRectMake(0, 0, screenWidth, 114);
//                    } else {
//                        self.revMobBannerAdView.frame = CGRectMake(0, 0, screenWidth, 50);
//                    }
//                        [[SNAdsManager getRootViewController].view addSubview:self.revMobBannerAdView];
//                });
                    CGSize size = [GenericAd currentSize];
//                    NSUInteger screenHeight = size.height;
//                    NSUInteger screenWidth = size.width;
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//                            self.revMobBannerAdView.frame = CGRectMake(250, 0, 640, 114);
                    } else {
//                            self.revMobBannerAdView.frame = CGRectMake(100, 0, 320, 50);
                    }
//                    self.revMobBannerAdView.hidden = NO;
//                    [[SNAdsManager getRootViewController].view addSubview:self.revMobBannerAdView];
//                    [[SNAdsManager getRootViewController].view bringSubviewToFront:self.revMobBannerAdView];
                });
            }
            @catch (NSException *exception) {
                NSLog(@"%@", exception.reason);
            }
            @finally {
                //
            }
        }
            break;
        case kAppLovin:
            dispatch_async(dispatch_get_main_queue(), ^{
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                } else {
                }
                });
            break;
        default:
            [NSException raise:@"Undefined Ad Network" format:@"Ad Network is unknown or does not have a banner Ad."];
            break;
    }
}

-(void)showBannerAd{
    switch(self.adNetworkType){
        case kRevMob:{
            @try {
                dispatch_async(dispatch_get_main_queue(), ^{
//                    self.revMobBannerAdView = [[RevMobAds session] bannerView];
//                    [self.revMobBannerAdView retain];
//                    self.revMobBannerAdView.delegate = self;
//                    [self.revMobBannerAdView loadAd];
//                    CGSize size = [GenericAd currentSize];
//                    NSUInteger screenHeight = size.height;
//                    NSUInteger screenWidth = size.width;
//                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//                        self.revMobBannerAdView.frame = CGRectMake(0, screenHeight-114, screenWidth, 114);
//                    } else {
//                        self.revMobBannerAdView.frame = CGRectMake(0, screenHeight-50, screenWidth, 50);
//                    }
//                    self.revMobBannerAdView.hidden = NO;
//                    [[SNAdsManager getRootViewController].view addSubview:self.revMobBannerAdView];
//                    [[SNAdsManager getRootViewController].view bringSubviewToFront:self.revMobBannerAdView];
               });
            }
            @catch (NSException *exception) {
                NSLog(@"%@", exception.reason);
            }
            @finally {
                //
            }
        }
            break;
    case kAppLovin:
            dispatch_async(dispatch_get_main_queue(), ^{
         
             
               // [NSTimer scheduledTimerWithTimeInterval:kAppLovinTimeOutThresholdValue target:self selector:@selector(adService:didFailToLoadAdWithError:) userInfo:nil repeats:NO];
            });
        break;
    default:
            [NSException raise:@"Undefined Ad Network" format:@"Ad Network is unknown or does not have a banner Ad."];
        break;
    }
}


-(void)showFullScreenAd{
    if (isAdVisible) {
        NSLog(@"Ad already visible");
        return;
    }
    NSLog(@"%s", __PRETTY_FUNCTION__);
    switch(self.adNetworkType){
        case kRevMob:
        {
            @try {
                if ([self.revMobFullScreenAd respondsToSelector:@selector(showAd)]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                    });
                }
            }
            @catch (NSException *exception) {
                NSLog(@"%@", exception.reason);
            }
        }
            break;
        case kAppLovin:{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (self.AppLovinTimer == nil) {
                
                    self.AppLovinTimer = [NSTimer scheduledTimerWithTimeInterval:kAppLovinTimeOutThresholdValue target:self selector:@selector(adService:didFailToLoadAdWithError:) userInfo:nil repeats:NO];
                    DebugLog(@"Scheduled AppLovin Timer ");
                }
                
                
            }); 
        }
            break;
        case kChartBoost:{
            dispatch_async(dispatch_get_main_queue(), ^{
                self.chartBoost.delegate = self;
                [self.chartBoost showInterstitial];
                //self.chartBoostTimer = [NSTimer scheduledTimerWithTimeInterval:kChartBoostTimeOutThresholdValue target:self selector:@selector(didFailToLoadInterstitial:) userInfo:nil repeats:NO];
            });
        }
            break;
        case kPlayHaven:{
            count++;
            NSLog(@"COunt for play HAVEN %d", count);
          //  dispatch_async(dispatch_get_main_queue(), ^{
                [self showPlayHavenFullScreenAd];
          //  });
            
        }
            break;
        default:
            [NSException raise:@"Undefined Ad Network" format:@"Ad Network is unknown or does not have a banner Ad."];
            break;
    }
}
-(void)showLinkButtonAd{
}
+(CGSize) sizeInOrientation:(UIInterfaceOrientation)orientation
{
    CGSize size = [UIScreen mainScreen].bounds.size;
    UIApplication *application = [UIApplication sharedApplication];
    if (UIInterfaceOrientationIsLandscape(orientation))
    {
        size = CGSizeMake(size.height, size.width);
    }
    if (application.statusBarHidden == NO)
    {
#ifdef IS_IOS7_AND_UP
        //return size;
#else
        size.height -= MIN(application.statusBarFrame.size.width, application.statusBarFrame.size.height);
#endif
    }
    return size;
}
+(CGSize) currentSize
{
    return [GenericAd sizeInOrientation:[UIApplication sharedApplication].statusBarOrientation];
}
-(void)hideBannerAd{
    
   
}

- (void)showPlayHavenFullScreenAd{
    DebugLog(@"%s", __PRETTY_FUNCTION__);
    DebugLog(@"YAAY PLAY HAVEN");
    dispatch_async(dispatch_get_main_queue(), ^{
    });
   
}



//TODO: This is not a good way to do this from what I can tell, but until 
- (void)revmobAdDidFailWithError:(NSError *)error{
    NSLog(@" Hey hey hey %s", __PRETTY_FUNCTION__);
    if (self.adType == kBannerAd) {
        NSLog(@"REVMOB BANNER FAILED");
        
            [self.delegate revMobBannerDidFailToLoad:self];
//            [self.revMobBannerAdTimer invalidate];
//            self.revMobBannerAdTimer = nil;
        
    }else if (self.adType == kFullScreenAd){
        NSLog(@"REVMOB FULLSCREEN FAILED");
            [self.delegate revMobFullScreenDidFailToLoad:self];
//            [self.revMobFullScreenAdTimer invalidate];
//            self.revMobFullScreenAdTimer = nil;
    }
}
- (void)revmobAdDisplayed{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    if (self.adType == kBannerAd) {
        [self.delegate revMobBannerDidLoadAd:self];

    }else if (self.adType == kFullScreenAd){
        isAdVisible = YES;
        [self.delegate revMobFullScreenDidLoadAd:self];
        self.isRevMobFullScreenAlreadyShown = YES;
    }
}
- (void)revmobUserClosedTheAd{
    if(self.adType == kFullScreenAd){
        self.isRevMobFullScreenAlreadyShown = NO;
        isAdVisible = NO;
    }
}
- (void)revmobUserClickedInTheAd{
    isAdVisible = NO;
    [self.revMobFullScreenAd hideAd];
}
- (void)revmobAdDidReceive{
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    if (self.adType == kBannerAd) {
        [self.delegate revMobBannerDidLoadAd:self];
    }else if (self.adType == kFullScreenAd){
        [self.delegate revMobFullScreenDidLoadAd:self];
    }
}
- (void)didFailToLoadInterstitial:(NSString *)location{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    // NSLog(@"%@",[NSThread callStackSymbols]);
    /**
     On every callback increment the callback count by one
     on second callback check if the difference between first and second call is more than 1.5 sec
     If its more than 4.5 than most probably its a genuine failure callback
     else if its less than 4.5 just ignore it
     To make things quicker and not having to calculate nsdate instances everytime we're placing them in if else statements with respect to the callback counters.
     */
    callBackCount++;
    if(callBackCount == 1){
       firstCallBackTime = [[NSDate date] timeIntervalSince1970];
        [self.delegate chartBoostFullScreenDidFailToLoad:self];
    }
    else if (callBackCount == 2){
        NSDate *now = [NSDate date];
        double end = [now timeIntervalSince1970];
        double difference = end - firstCallBackTime;
        NSLog(@"Difference between calls is %f", difference);
        if (difference > 7.5) {
            [self.delegate chartBoostFullScreenDidFailToLoad:self];
        }
    }else{
        [self.delegate chartBoostFullScreenDidFailToLoad:self];
    }
        
    
}
- (BOOL)shouldDisplayInterstitial:(NSString *)location{
    isAdVisible = YES;
    return YES;
}
- (BOOL)shouldDisplayLoadingViewForMoreApps{
    return YES;
}

- (void)didCloseInterstitial:(NSString *)location{
    isAdVisible = NO;
}
- (void)didClickInterstitial:(NSString *)location{
    
}

#pragma mark -
#pragma mark Play Haven


#pragma mark -
#pragma mark AppLovin Methods
@end
