//
//  SNManager.h
//  Alien Tower
//
//  Created by Asad Khan on 05/02/2013.
//
//

#import <Foundation/Foundation.h>

#import <Availability.h>

/*
 #if !__has_feature(objc_arc)
 #error This class requires automatic reference counting
 #endif
 */
#define IS_IOS7_AND_UP ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0)
#ifdef DEBUG
#define DebugLog(f, ...) NSLog(f, ## __VA_ARGS__)
#else
#define DebugLog(f, ...)
#endif

#define FreeApp

#define kPlayHavenAdTimeOutThresholdValue 4.0
#define kRevMobAdTimeOutThresholdValue 3.0
#define kAppLovinTimeOutThresholdValue 5.0
#define kChartBoostTimeOutThresholdValue 5.0
#ifdef FreeApp
#define kRevMobId @"53d7f09aaf69ad95475a348d"

#define ChartBoostAppID @"53cc0bfc1873da2c64f64e3f"
#define ChartBoostAppSignature @"910dc416c500654647d22df37cc665cf6ecdb9b9"

#define kAppLovinID @"1o1k_qTSquWFa0IG3PnU2c0OgWfD9A7Bz9vyzSPoLw1wwJB-kzTnaT9F_qT8-bJylDuhfIT_CUw7p56W3ZHd2R"

#define kPlayHavenAppToken @"7a8e822487b44feb9a2e9e9c3c841059"
#define kPlayHavenSecret @"9f2acb4b5ac749b78e47b46bcb670ebe"
#define kPlayHavenPlacement @"boys_landscape_placements"
#ifdef IS_IOS7_AND_UP
#define kRateURL @"itms-apps://itunes.apple.com/app/id907323715"
#else
#define kRateURL @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=907323715&at=10lIwv"
#endif

#endif

#ifdef PaidApp
#define kRevMobId @"53d7f09aaf69ad95475a348d"

#define ChartBoostAppID @"53cc0bfc1873da2c64f64e3f"
#define ChartBoostAppSignature @"910dc416c500654647d22df37cc665cf6ecdb9b9"

#define kPlayHavenAppToken @"7a8e822487b44feb9a2e9e9c3c841059"
#define kPlayHavenSecret @"9f2acb4b5ac749b78e47b46bcb670ebe"
#define kPlayHavenPlacement @"boys_landscape_placements"

#define kTapJoyAppID @""
#define kTapJoySecretKey @""


#ifdef IS_IOS7_AND_UP
    #define kRateURL @"itms-apps://itunes.apple.com/app/id907323715"
#else
    #define kRateURL @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=907323715&at=10lIwv"
#endif

#endif



typedef NS_ENUM(NSUInteger, adPriorityLevel){
    kPriorityLOWEST = 10,
    kPriorityNORMAL,
    kPriorityHIGHEST
};

typedef NS_ENUM(NSUInteger, ConnectionStatus) {
    
    kNotAvailable,
    kWANAvailable,
    kWifiAvailable
    
};

/*
 These are the default values before changing them do consult Angela
 */
#define kRevMobBannerAdPriority kPriorityLOWEST  //In Game banner Ads
#define kRevMobFullScreenAdPriority kPriorityLOWEST //Full Screen Pop-ups
#define kRevMobButtonAdPriority kPriorityLOWEST //Button ads this is not currently used in games, its just a wrapper on Link Ads
#define kRevMobLinkAdPriority kPriorityLOWEST  //This is the Ad that is displayed on buttons on game over screens
#define kRevMobPopAdPriority kPriorityLOWEST  //UIAlert type pop-up Ads in games
#define kRevMobLocalNotificationAdPriority kPriorityLOWEST // UILocalNotification Ads //Currently we're not using it

#define kChartBoostFullScreeAdPriority kPriorityNORMAL
#define kChartBoostMoreAppsAd kPriorityNORMAL

//#define kMobClixBannerAdPriority -1
//#define kMobClixFullScreenAdPriority -1


#define kPlayHavenFullScreenAdPriority kPriorityNORMAL

#define kAppLovinBannerAdPriority kPriorityHIGHEST
#define kAppLovinFullScreenAdPriority kPriorityHIGHEST

#define kNumberOfAdNetworks 4





@interface SNManager : NSObject



@end
